package com.yuttana.lab6;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class BookBankTest {
    @Test
    public void shouldWithdrawSuccess() {
        BookBank book =  new BookBank("Yuttana", 100);
        book.withdraw(50);
        assertEquals(50, book.balance, 0.0001);
    }

    @Test
    public void shouldWithdrawOverBalance() {
        BookBank book =  new BookBank("Yuttana", 100);
        book.withdraw(150);
        assertEquals(100, book.balance, 0.0001);
    }

    @Test
    public void shouldWithdrawWithNagativeNumber() {
        BookBank book =  new BookBank("Yuttana", 100);
        book.withdraw(-100);
        assertEquals(100, book.balance, 0.0001);
    }

    @Test
    public void shouldDepositWithNagativeNumber() {
        BookBank book =  new BookBank("Yuttana", 100);
        book.deposit(-100);
        assertEquals(100, book.balance, 0.0001);
    }
}
