package com.yuttana.lab6;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class RobotTest {
    @Test
    public void shouldDownOver() {
        Robot robot = new Robot("Robot", 'R', 0, Robot.MAX_Y);
        assertEquals(false, robot.down());
        assertEquals(Robot.MAX_Y, robot.getY());
    }

    @Test
    public void shouldUpNagative() {
        Robot robot = new Robot("Robot", 'R', 0, Robot.MIN_Y);
        assertEquals(false, robot.up());
        assertEquals(Robot.MIN_Y, robot.getY());
    }

    @Test
    public void shouldDownSeccess() {
        Robot robot = new Robot("Robot", 'R', 0, 0);
        assertEquals(true, robot.down());
        assertEquals(1, robot.getY());
    }

    @Test
    public void shouldUpSeccess() {
        Robot robot = new Robot("Robot", 'R', 0, 1);
        assertEquals(true, robot.up());
        assertEquals(0, robot.getY());
    }

    @Test
    public void shouldRightSeccess() {
        Robot robot = new Robot("Robot", 'R', 0, 0);
        assertEquals(true, robot.right());
        assertEquals(1, robot.getX());
    }

    @Test
    public void shouldleftSeccess() {
        Robot robot = new Robot("Robot", 'R', 1, 0);
        assertEquals(true, robot.left());
        assertEquals(0, robot.getX());
    }

    @Test
    public void shouldleftNagative() {
        Robot robot = new Robot("Robot", 'R', Robot.MIN_X, 0);
        assertEquals(false, robot.left());
        assertEquals(Robot.MIN_X, robot.getX());
    }

    @Test
    public void shouldRightOver() {
        Robot robot = new Robot("Robot", 'R', Robot.MAX_X, 0);
        assertEquals(false, robot.right());
        assertEquals(Robot.MAX_X, robot.getX());
    }
}
