package com.yuttana.lab6;

public class BookBank {
    String name;
    double balance;
    public BookBank (String name, double balance) {
        this.name = name;
        this.balance = balance;
    }
    boolean deposit(double money) {
        if(money<1) return false;
        this.balance = this.balance + money;
        return true;
    }
    boolean withdraw(double money) {
        if(money>balance) return false;
        if(money<1) return false;
        this.balance = this.balance - money;
        return true;
    }

    void print() {
        System.out.println(name + " " + balance);
    }
}
