package com.yuttana.lab6;

public class Robot {
    private String name;
    private char symbol;
    private int x;
    private int y;
    public static final int MIN_X = 0;
    public static final int MIN_Y = 0;
    public static final int MAX_X = 19;
    public static final int MAX_Y = 19;

    public Robot(String name, char symbol, int x, int y) {
        this.name = name;
        this.symbol = symbol;
        this.x = x;
        this.y = y;
    }
    public  boolean up() {
        if(y == MIN_Y) return false;
        y = y-1;
        return true;
    }
    public  boolean down() {
        if(y == MAX_Y) return false;
        y = y+1;
        return true;
    }
    public  boolean left() {
        if(x == MIN_X) return false;
        x = x-1;
        return true;
    }
    public  boolean right() {
        if(x == MAX_X) return false;
        x = x+1;
        return true;
    }
    public void print(){
        System.err.println("Robot: " + name + " X: " + x + "Y: " + y);
    }
    public String getName() {
        return name;
    }
    public char getSymbol() {
        return symbol;
    }
    public int getX() {
        return x;
    }
    public int getY() {
        return y;
    }
}
